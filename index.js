//setup dependencies
const express = require("express");
const mongoose = require("mongoose");

//this allows us to use all the routes defined in taskRoutes.js
const taskRoutes = require("./routes/taskRoutes");

//Server setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database connection
//Connecting to MongoDB Atlas
mongoose.set("strictQuery", true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.vx1mcmz.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

//Connection to DB
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"));


//Add the task route
//allows all the task routes created in the taskRoutes.js file to use "/tasks" route
app.use("/tasks", taskRoutes)


//Server listening
app.listen(port,()=>console.log(`Now listening to ${port}`))